import React, { useState, useEffect } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import useFormState from './hooks/useFormState';
import {useCookies} from 'react-cookie';
import axios from 'axios';

function RegisterForm(props) {

    const navigate = useNavigate();
    const [username, updateUsername, resetUsername] = useFormState("");
    const [firstname, updateFirstname, resetFirstname] = useFormState("");
    const [lastname, updateLastname, resetLastname] = useFormState("");
    const [email, updateEmail, resetEmail] = useFormState("");
    const [password, updatePassword, resetPassword] = useFormState("");   

    const [cookie, setCookie, removeCookie] = useCookies(['auth_token']);

    useEffect(() => {
        if (cookie['auth_token']) navigate("/");
    }, []);

    const register = (e) => {
        e.preventDefault();
        axios.post("http://localhost:8080/register", null, {
            params: {
                username: username,
                firstname: firstname,
                lastname: lastname,
                email: email,
                password: password
            }
        }).then(res => {
            // reset input fields
            setCookie("auth_token", res.data.jwt);
            navigate("/");
        }).catch(err => {
            resetUsername();
            resetFirstname();
            resetLastname();
            resetEmail();
            resetPassword();
        });
    }

    return (
        <>
        <form className="RegisterForm" onSubmit={register}>
            <div>
                <label htmlFor="username">Username</label>
                <input name="username" id="username" type="text" required value={username} onChange={updateUsername}/>
            </div>
            <div>
                <label htmlFor="firstname">First Name</label>
                <input name="firstname" id="firstname" type="text" required value={firstname} onChange={updateFirstname}/>
            </div>
            <div>
                <label htmlFor="lastname">Last Name</label>
                <input name="lastname" id="lastname" type="text" required value={lastname} onChange={updateLastname}/>
            </div>
                <label htmlFor="email">E-Mail</label>
                <input name="email" id="email" type="email" required value={email} onChange={updateEmail}/>
            <div>
                <label htmlFor="password">Password</label>
                <input name="password" id="password" type="password" required value={password} onChange={updatePassword}/>
            </div>
            <button type="submit">Sign Up</button>
        </form>
        <Link to="/login">Already have an acoount?</Link>
        </>
    );
}
export default RegisterForm;