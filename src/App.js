import './App.css';
import LogInForm from './LogInForm';
import { Route, Routes } from "react-router-dom";
import AppView from './AppView';
import { useCookies } from 'react-cookie';
import { useEffect } from 'react';
import RegisterForm from './RegisterForm';

function App() {

  return (
    <div className="App">
      <Routes>
        <Route path="/" exact element={<AppView/>} />
        <Route path="/login" exact element={<LogInForm/>} />
        <Route path="/register" exact element={<RegisterForm/>}/>
      </Routes>
    </div>
  );
}
export default App;
