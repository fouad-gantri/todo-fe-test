import React, { useEffect, useState } from 'react';
import useFormState from './hooks/useFormState';
import {useNavigate, Link} from 'react-router-dom';
import axios from 'axios';
import {useCookies} from 'react-cookie';

function LogInForm(props) {

    const navigate = useNavigate();
    const [username, updateUsername, resetUsername] = useFormState("");
    const [password, updatePassword, resetPassword] = useFormState("");
    const [badCredentialsMsg, setBadCredentialsMsg] = useState(false);
    
    const [cookie, setCookie, removeCookie] = useCookies(['auth_token']);

    useEffect(() => {
        if (cookie['auth_token']) navigate("/");
    }, []);

    const handleSubmit = async (e) => {
        e.preventDefault();
        try {
            const res = await axios.post("http://localhost:8080/authenticate", { 
                username: username,
                password: password
            });
            const jwtToken = res.data.jwt;
            //console.log(jwtToken);
            //console.log(
            //    "%cUser successfully logged in!",
            //    "background-color: #43a047; color: white; font-size: 22px; padding: 10px 20px"
            //);
            setCookie("auth_token", jwtToken);
            navigate("/")
        } catch (error) {
            //console.log(
            //    "%cLog In wasn't successful!",
            //    "background-color: #e53935; color: white; font-size: 22px; padding: 10px 20px"
            //);
            console.log(error);
            resetUsername();
            resetPassword();
            setBadCredentialsMsg(true);
        }
        

    }

    return (
        <>
        {badCredentialsMsg &&
            <div style={{color: "red"}}>
                Wrong Username or Password!
            </div>
        }
        <form className="LogInForm" onSubmit={handleSubmit}>
            <div>
                <label htmlFor="username">Username</label>
                <input id="username" name="username" type="text" value={username} onChange={updateUsername} />
            </div>
            <div>
                <label htmlFor="password">Password</label>
                <input id="password" name="password" type="password" value={password} onChange={updatePassword} />
            </div>
            <button type="submit">Log In</button>
        </form>
        <Link to="/register">I don't have an account</Link>
        </>
    );
}
export default LogInForm;