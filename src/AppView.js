import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router';
import {useCookies} from 'react-cookie';

function AppView(props) {

    const navigate = useNavigate();
    //const access_token = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJkZXZiYWJiYWdlIiwiZXhwIjoxNjM4NDczMjA5LCJpYXQiOjE2Mzg0MzcyMDl9.Zp7d8X9FJr0VNylZENlRMTt8Ze3JrcYvsIlwD7Nz618";
    const [userDetails, setUserDetails] = useState(null);
    const [loading, setLoading] = useState(true);
    
    const [cookie, setCookie, removeCookie] = useCookies(['auth_token']);

    useEffect(() => {
        axios.get("http://localhost:8080/user", {
            headers: {
                'Authorization': `Bearer ${cookie["auth_token"]}`
            }
        }).then(res => {
            setUserDetails(res.data)
            setLoading(false);
        }).catch(err => {
            if (cookie["auth_token"]) removeCookie("auth_token"); 
            navigate("/login");
        });
        
    }, []);

    const renderUser = () => {
        if (!userDetails) return [];
        const userDetailsArr = [];
        Object.keys(userDetails).forEach(detail => {
            userDetailsArr.push(<li>{`${detail}: ${userDetails[detail]}`}</li>);
        });
        return userDetailsArr;
    }

    const logout = () => {
        removeCookie("auth_token");
        navigate("/login");
    }

    return (
        !loading ?
        (
        <div className="AppView">
            <ul>
                {renderUser()}
            </ul>
            <form onSubmit={logout}>
                <button type="submit">Log Out</button>
            </form>
        </div>
        ):(
        <div>
            loading...
        </div>
        )
    );
}
export default AppView;